package beckselAndroid.com;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class AdapterMedecin extends BaseAdapter {

    private Context context;
    private LayoutInflater inflater;
    private String[] medecin;

    public AdapterMedecin(Context context, String [] user){
        this.context=context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.medecin = medecin;
    }

    @Override
    public int getCount() {
        return medecin.length;
    }

    @Override
    public Object getItem(int position) {
        return medecin[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.line_medecin, null);
        TextView medecinTextView = convertView.findViewById(R.id.medecinTextView);
        medecinTextView.setText(medecin[position]);
        return convertView;
    }
}
