package beckselAndroid.com;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

public class Rdv extends AppCompatActivity {
    ListView lv;

    private String dataRdv = "";
    private String[] rdvs;
    AdapterRdv adapterRdv;

    public static final String MSG_UTILISATEUR="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rdv);

        final DatabaseAccess db = DatabaseAccess.getInstance(getApplicationContext());
        db.open();

        rdvs = db.getRdvs();
        lv = findViewById(R.id.visiteurList);

        adapterRdv = new AdapterRdv(this, rdvs);
        lv.setAdapter(adapterRdv);
        lv.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent = new Intent(Rdv.this, Rdv.class);
            intent.putExtra(Rdv.MSG_UTILISATEUR, rdvs[position]);
            startActivity(intent);
        });



        db.close();

    }


}