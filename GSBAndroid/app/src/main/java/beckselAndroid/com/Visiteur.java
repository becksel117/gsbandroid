package beckselAndroid.com;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

public class Visiteur extends AppCompatActivity {
    ListView lv;

    private String dataVisiteur = "";
    private String[] visiteurs;
    AdapterVisiteur adapterVisiteur;

    public static final String MSG_UTILISATEUR="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visiteur);

        final DatabaseAccess db = DatabaseAccess.getInstance(getApplicationContext());
        db.open();

        visiteurs = db.getVisiteurs();
        lv = findViewById(R.id.visiteurList);

        adapterVisiteur = new AdapterVisiteur(this, visiteurs);
        lv.setAdapter(adapterVisiteur);
        lv.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent = new Intent(Visiteur.this, Visiteur.class);
            intent.putExtra(Visiteur.MSG_UTILISATEUR, visiteurs[position]);
            startActivity(intent);
        });



        db.close();

    }


}