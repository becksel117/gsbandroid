package beckselAndroid.com;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class AdapterRdv extends BaseAdapter {

    private Context context;
    private LayoutInflater inflater;
    private String[] rdv;

    public AdapterRdv(Context context, String [] user){
        this.context=context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.rdv = rdv;
    }

    @Override
    public int getCount() {
        return rdv.length;
    }

    @Override
    public Object getItem(int position) {
        return rdv[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.line_rdv, null);
        TextView rdvTextView = convertView.findViewById(R.id.rdvTextView);
        rdvTextView.setText(rdv[position]);
        return convertView;
    }
}
