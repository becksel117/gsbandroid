package beckselAndroid.com;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

public class Medecin extends AppCompatActivity {
    ListView lv;

    private String dataMedecin = "";
    private String[] medecins;
    AdapterMedecin adapterMedecin;

    public static final String MSG_UTILISATEUR="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medecin);

        final DatabaseAccess db = DatabaseAccess.getInstance(getApplicationContext());
        db.open();

        medecins = db.getMedecins();
        lv = findViewById(R.id.medecinList);

        adapterMedecin = new AdapterMedecin(this, medecins);
        lv.setAdapter(adapterMedecin);
        lv.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent = new Intent(Medecin.this, Medecin.class);
            intent.putExtra(Medecin.MSG_UTILISATEUR, medecins[position]);
            startActivity(intent);
        });



        db.close();

    }


}