package beckselAndroid.com;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

public class Produit extends AppCompatActivity {
    ListView lv;

    private String dataProduit = "";
    private String[] produits;
    AdapterProduit adapterProduit;

    public static final String MSG_UTILISATEUR="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_produit);

        final DatabaseAccess db = DatabaseAccess.getInstance(getApplicationContext());
        db.open();

        produits = db.getProduits();
        lv = findViewById(R.id.produitList);

        adapterProduit = new AdapterProduit(this, produits);
        lv.setAdapter(adapterProduit);
        lv.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent = new Intent(Produit.this, Produit.class);
            intent.putExtra(Produit.MSG_UTILISATEUR, produits[position]);
            startActivity(intent);
        });



        db.close();

    }


}