package beckselAndroid.com;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DatabaseAccess {
    private DatabaseOpenHelper openHelper;
    private SQLiteDatabase db;
    private static DatabaseAccess instance;
    Cursor c = null;

    private DatabaseAccess(Context context) { this.openHelper = new DatabaseOpenHelper(context); }

    public static DatabaseAccess getInstance(Context context){
        if(instance==null){
            instance = new DatabaseAccess(context);
        }
        return instance;
    }

    public void open() { this.db=openHelper.getWritableDatabase(); }
    public void close(){
        if(db!=null){
            this.db.close();
        }
    }

   public String[] getVisiteurs(){
        c=db.rawQuery("select * from  Visiteur", new String[]{});
        String[] visiteurs = new String[c.getCount()];
        int i = 0;
        while(c.moveToNext()){
            visiteurs[i]=c.getString(0);
            i++;
        }
        return visiteurs;
   }

    public String[] getMedecins(){
        c=db.rawQuery("select * from  Medecin", new String[]{});
        String[] medecins = new String[c.getCount()];
        int i = 0;
        while(c.moveToNext()){
            medecins[i]=c.getString(0);
            i++;
        }
        return medecins;
    }

    public String[] getProduits(){
        c=db.rawQuery("select * from  Produit", new String[]{});
        String[] produits = new String[c.getCount()];
        int i = 0;
        while(c.moveToNext()){
            produits[i]=c.getString(0);
            i++;
        }
        return produits;
    }

    public String[] getRdvs(){
        c=db.rawQuery("select * from  Rdv", new String[]{});
        String[] rdvs = new String[c.getCount()];
        int i = 0;
        while(c.moveToNext()){
            rdvs[i]=c.getString(0);
            i++;
        }
        return rdvs;
    }

}
