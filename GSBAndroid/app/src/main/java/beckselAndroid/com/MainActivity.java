package beckselAndroid.com;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button visiteur;
    private Button medecin;
    private Button produit;
    private Button rdv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.visiteur = findViewById(R.id.visiteur);
        this.medecin = findViewById(R.id.medecin);
        this.produit = findViewById(R.id.produit);
        this.rdv = findViewById(R.id.rdv);
        visiteur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent otherActivity = new Intent(getApplicationContext(), Visiteur.class);
                startActivity(otherActivity);
                finish();
            }

        });
        medecin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent otherActivity = new Intent(getApplicationContext(), Medecin.class);
                startActivity(otherActivity);
                finish();
            }

        });
        produit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent otherActivity = new Intent(getApplicationContext(), Produit.class);
                startActivity(otherActivity);
                finish();
            }

        });
        rdv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent otherActivity = new Intent(getApplicationContext(), Rdv.class);
                startActivity(otherActivity);
                finish();
            }

        });
    }
}