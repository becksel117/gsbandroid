package beckselAndroid.com;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class AdapterProduit extends BaseAdapter {

    private Context context;
    private LayoutInflater inflater;
    private String[] produit;

    public AdapterProduit(Context context, String [] user){
        this.context=context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.produit = produit;
    }

    @Override
    public int getCount() {
        return produit.length;
    }

    @Override
    public Object getItem(int position) {
        return produit[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.line_produit, null);
        TextView produitTextView = convertView.findViewById(R.id.produitTextView);
        produitTextView.setText(produit[position]);
        return convertView;
    }
}
